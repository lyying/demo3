import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'One',
    component: () => import(/* webpackChunkName: "One" */ '../views/One.vue')
  },
  {
    path:'/tow',
    name:'Tow',
    component: () => import(/* webpackChunkName: "tow" */ '../views/Tow.vue')
  },
  {
    path:'/three',
    name:'Three',
    component: () => import(/* webpackChunkName: "three" */ '../views/Three.vue')
  },
  {
    path:'/Four',
    name:"Four",
    component:() => import(/* webpackChunkName: "four" */ '../views/Four.vue')
  },
  {
    path: '/five',
    name: 'Five',
    component: () => import(/* webpackChunkName: "five" */ '../views/Five.vue')
  },
  
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  routes
})

export default router
